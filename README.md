# CI example for an FPGA project

When there are no suitable resources for FPGA supported CI one often needs to setup a **mirror repository**. When push and pull mirrors are not possible due to firewall or license limitations one can in addition use a local proxy repository.

### Setup local proxy repository

```bash
git clone --mirror git@gitlab.com:MAXIV-SCISW/compute-fpgas/ci-mirror.git
cd ci-mirror.git
git remote set-url --push origin git@gitlab.maxiv.lu.se:compute-fpgas/ci-mirror.git
```

### Proxy repo usage

```bash
# in ci-mirror.git directory
git fetch -p origin
git push --mirror
```

### Troubleshooting
- [![remote rejected] errors after mirroring a git repository](https://stackoverflow.com/questions/34265266/remote-rejected-errors-after-mirroring-a-git-repository)

### References
- [Github Documentation](https://docs.github.com/en/repositories/creating-and-managing-repositories/duplicating-a-repository)
