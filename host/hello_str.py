#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import absolute_import, print_function
import pyopencl as cl
import numpy as np
import os, sys

fpga_emulation=os.getenv('CL_CONTEXT_EMULATOR_DEVICE_INTELFPGA')=='1' or os.getenv('CL_CONFIG_CPU_EMULATE_DEVICES')=='1'

# load bitstream kernel
if(fpga_emulation):
        try:
                platform = [p for p in cl.get_platforms() if 'Intel(R) FPGA Emulation' in p.name][0]
        except:
                if os.getenv('CL_CONTEXT_EMULATOR_DEVICE_INTELFPGA')=='1': # 17.1 legacy behaviour
                        platform = [p for p in cl.get_platforms() if 'Intel(R) FPGA SDK' in p.name][0]
                else:
                        raise
        with open("bin/hello_str-em.aocx", "rb") as fid:
                cl_binary = fid.read()
else:
	platform = [p for p in cl.get_platforms() if 'Intel(R) FPGA SDK' in p.name][0]
	with open("bin/hello_str.aocx", "rb") as fid:
		cl_binary = fid.read()

ctx = cl.Context(
        dev_type=cl.device_type.ALL,
        properties=[(cl.context_properties.PLATFORM, platform)])


queue = cl.CommandQueue(ctx)

mf = cl.mem_flags

str_h = np.array(list(b'\0'*128), dtype=np.byte)
str_g = cl.Buffer(ctx, mf.COPY_HOST_PTR, hostbuf=str_h)

prg = cl.Program(ctx, ctx.devices, [cl_binary,]).build()

# === test 1 ===

#krn = prg.hello_str(queue, (1,), None, str_g)
krn = prg.hello_str
if(krn.num_args==3):
        print("WARNING: OpenCL returns wrong number of kernel arguments, enforcing num_args=%d" % (krn.num_args-2,))
        krn.set_enforce_num_args(prg, krn.num_args-2)
krn.set_args(str_g)
cl.enqueue_nd_range_kernel(queue, krn, (1,1,1), (1,1,1))

cl.enqueue_copy(queue, str_h, str_g)

# print
print(str_h.tobytes().decode())

# === test 2 ===

#krn = prg.hello_str_lowercase(queue, (1,), None, str_g)
krn = prg.hello_str_lowercase
if(krn.num_args==3):
        print("WARNING: OpenCL returns wrong number of kernel arguments, enforcing num_args=%d" % (krn.num_args-2,))
        krn.set_enforce_num_args(prg, krn.num_args-2)
krn.set_args(str_g)
cl.enqueue_nd_range_kernel(queue, krn, (1,1,1), (1,1,1))

cl.enqueue_copy(queue, str_h, str_g)

# print
print(str_h.tobytes().decode())

# ------------------------------------------------------------------------
# Usage: PYOPENCL_COMPILER_OUTPUT=1 PYOPENCL_CTX='0' python host/hello_str.py

