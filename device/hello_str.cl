#pragma OPENCL EXTENSION cl_khr_byte_addressable_store : enable
/* Note: This implementation is very ineffective on FPGA.
         It should demonstrate several other concepts:
         - using _global char* with pyopencl
	 - using NDrange, workgroups, etc.
	 - having (constant/global) global scope arrays
*/

__constant char hello_string[14]     = {'H','e','l','l','o',' ','W','o','r','l','d','!','\n'};
__constant char hello_string_low[14] = {'h','e','l','l','o',' ','w','o','r','l','d','!','\n'};

__attribute__((task)) __kernel void hello_str(__global char* string)
{
  #pragma unroll
  for(uint idx=0; idx<14; idx++)   
    string[idx] = hello_string[idx];
}

__attribute__((task)) __kernel void hello_str_lowercase(__global char* string)
{
  #pragma unroll
  for(uint idx=0; idx<14; idx++)
    string[idx] = hello_string_low[idx];
}
